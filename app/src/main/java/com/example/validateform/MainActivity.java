package com.example.validateform;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText efn, eln, eem, eun, ep, erp;
    String firstname, lastname, mail, username, password, confpassword;
    Button reg, clr;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        efn = (EditText) findViewById(R.id.fname);
        eln = (EditText) findViewById(R.id.lname);
        eem = (EditText) findViewById(R.id.uemail);
        eun = (EditText) findViewById(R.id.uname);
        ep = (EditText) findViewById(R.id.pass);
        erp = (EditText) findViewById(R.id.rpass);

        reg = (Button) findViewById(R.id.btn1);
        clr = (Button) findViewById(R.id.btn2);

        reg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resister();
            }
        });
        clr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                efn.setText("");
                eln.setText("");
                eem.setText("");
                eun.setText("");
                ep.setText("");
                erp.setText("");
            }
        });
    }





    public void resister() {
        initialize();
        if (!validate()) {
            Toast.makeText(this, "User Resistration failed !!", Toast.LENGTH_SHORT).show();
        } else {
            onResisterSuccess();
        }
    }

    private void initialize() {
        firstname = efn.getText().toString().trim();
        lastname = eln.getText().toString().trim();
        mail = eem.getText().toString().trim();
        username = eun.getText().toString().trim();
        password = ep.getText().toString().trim();
        confpassword = erp.getText().toString().trim();
    }

   /* private void initialize() {
        firstname = efn.getText().toString().trim();
        lastname = eln.getText().toString().trim();
        mail = eem.getText().toString().trim();
        username = eun.getText().toString().trim();
        password = ep.getText().toString().trim();
        confpassword = erp.getText().toString().trim();
    }*/

    public void onResisterSuccess() {

        Toast.makeText(this, "User Resistration Successfull !!", Toast.LENGTH_SHORT).show();
        efn.setText("");
        eln.setText("");
        eem.setText("");
        eun.setText("");
        ep.setText("");
        erp.setText("");
    }

    public boolean validate() {

        boolean val = true;
        if(firstname.isEmpty()){
            efn.setError("!! First Name cant be blank");
            efn.requestFocus();
            val=false;
        }
        if(lastname.isEmpty()){
            eln.setError("!! Last Name cant be blank");
            eln.requestFocus();
            val=false;
        }
        if(mail.isEmpty()){
            eem.setError("!! Mail cant be blank");
            eem.requestFocus();
            val=false;
        }
        if(username.isEmpty()){
            eun.setError("!! User Name cant be blank");
            eun.requestFocus();
            val=false;
        }
        if(password.isEmpty()){
            ep.setError("!! Password cant be blank");
            ep.requestFocus();
            val=false;
        }
        if(confpassword.isEmpty()){
            erp.setError("!! Confirm password cant be blank");
            erp.requestFocus();
            val=false;
        }
        if(mail.isEmpty()){
            eem.setError("Please, Enter valid E-mail Address");
            eem.requestFocus();
            val=false;

        }
        if(password.length()<8){
            ep.setError("Minimum password length is 8");
            ep.requestFocus();
            val=false;

        }
        if(confpassword.length()<8 || !password.equals(confpassword)){
            erp.setError("Password doesn't match");
            erp.requestFocus();
            val=false;

    }
        return val;


}
}